from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

def todo_list_list(request):
    todolists = TodoList.objects.all()
    context = {
        "todolist_list": todolists,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todolist_object": todolist,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todolist)
    context = {
        "todolist_object": todolist,
        "form": form,
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    todolist_id = TodoList.objects.get(id=id)
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist_id.delete()
        return redirect("todo_list_list")
    context = {
        "todolist_object": todolist,
    }
    return render(request, "todos/delete.html", context)

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/items/create.html", context)

def todo_item_update(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=todoitem)
    context = {
        "todoitem_object": todoitem,
        "form": form,
    }
    return render(request, "todos/items/edit.html", context)
